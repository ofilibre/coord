![](Pictures/10000000000004B000000320C22CB0FD08FB8379.jpg)

# Propuesta de sede para el próximo congreso nacional de software libre y cultura abierta

Salvo detalles particulares de la convocatoria y cambios mínimos  este documento incluye copia de la propuesta ganadora para la sede de [LibOCon 2019](https://libocon.org/)  (el encuentro anual internacional de la comunidad LibreOffice)a celebrar en Almería.

## Detalle de la propuesta

### Fechas

La propuesta almeriense se realiza a la sazón de la celebración del encuentro LibOConf en fechas inmediatamente antes o después del mismo, facilitando las labores de organización:

- fin de semana: 7-8 de septiembre

- lunes-martes: 9-10 de septiembre

- fin de semana: 14-15

### Espacios

En función del diseño final de contenidos pueden usarse uno o varios espacios. Se asume una asistencia mínima de 200 personas y una máxima de aproximadamente 300.

- una única sección plenaria de contenidos: auditorio (300 asientos)

- para varias secciones Aulario A-IV:

  - sala de grados principal (100 asientos)

  - hasta 20 aulas de 35 asientos

  - hall

También podrían usarse ambos espacios, que es una combinación muy habitual en congresos de comunidades FLOSS.

### Denominación del encuentro

Con la vista puesta en futuras repeticiones anuales proponemos el nombre «__de Par en Par__». ¿Por qué? En muchos sentidos estamos recuperando el espíritu, comunidad y valores de los viejos congresos HispaLinux que por entonces sirvieron de revulsivo para una comunidad ávida e inquieta y antecedido la actual abundante dinámica de encuentros y congresos tecnológicos por toda España de la actualidad. Yo mismo fui promotor, colaborador u organizador de aquellos encuentros.

Hoy por tanto parecería muy apropiado recuperar aquel nombre. Personalmente creo que ya no es adecuado:

- la denominación HispaLinux «está quemada», la última temporada activo de la asociación de la que tomó el nombre estuvo marcada por un declive a la vista de todos, algo normal y, a pesar, razonable;

- sin embargo desde que la ¿última? junta directiva se hizo cargo precipitaron la asociación hacia la absoluta irrelevancia, incluso cerrando servicios disponibles a los socios y, lo más terrible, interrumpiendo la comunicación con el grueso de los asociados y por extensión la actividad de representación democrática sin asambleas generales públicamente conocidas ni otras acciones relevantes; en mi opinión lo más sensato es alejarse de esas personas y en mi fuero interno sólo desearía _castigarlos con el látigo de la indiferencia_;

- en mi opinión la marca «Linux» ya no tiene la fuerza e impacto de, especialmente, la primera década de los 2000; en cambio se ha «commoditizado»: ya no puede parecer tan sectario para algunos como lo fue en el pasado, está tan ampliamente adoptado en la industria que prácticamente tales tecnologías se dan por supuestas en la mayoría de los ámbitos de las TIC; y esto no es menos que maravilloso, pero como marca o denominación ya no le aprecio el gancho rupturista del pasado;

- además, la evolución de las comunidades FLOSS ya va mucho más lejos que los ámbitos de los sistemas Linux, los sistemas operativos, la comunidad GNU, etc, etc: no sólo hay cantidades ingentes de productos software libres que corren en otros sistemas (Android, Windows, iPhone...), que están apadrinadas en comunidades estrictamente no relacionadas con Linux (algunas inmensas como Apache o Eclipse), otras en pleno auge, transversales, alrededor de lenguajes y marcos de programación sino que excede al mundo del software a los contenidos libres y abiertos que van desde Wikipedia, Creative Commons, OpenStreetMap... hasta las cada vez más abundantes fuentes de datos abiertos;

- y en la propia evolución de la actividad de HispaLinux ya hubo un cambio de foto importantísimo: hacia la protección de los derechos digitales y los marcos legales para la sociedad digital: tanto para construir un patrimonio común de software como para construir procomunes como la innovación (luchas contra las patentes software), la seguridad en las TIC, privacidad y anónimato personales en Internet, etc;

- finalmente, la denominación HispaLinux es muy reconocida e incluso querida por quienes vivimos aquellos tiempos más intensamente... y ya no somos los más jóvenes; sin querer renunciar a este nuestro público propongo abrirse a todo el público actual, más grande, preparado y diverso que nunca.

¿Por qué «__de Par en Par__»?

- porque es un encuentro de la comunidad, por la comunidad y para la comunidad;

- porque la mejor meritocracia del hackerismo se basa en la igualdad y así nos relacionamos, así colaboramos: entre pares;

- y porque de par en para es estar abierto: abiertos a los marcos de propiedad intelectual y reúso que creemos justos e imprescindibles para la sociedad digital actual, a todos los productos digitales e intelectuales creados en dichos marcos y porque como comunidad estamos abiertos a nuevas incorporaciones: no hay cooptación, eres uno más porque lo deseas.

Somos iguales. Transversales. Todo es abierto. Vivimos de par en par.

---



## «Almería, the city where the sun goes through the winter»

### Motivation

Some of us feels deeply love for the opensource communities and
practices, adopting them in our professional careers indeed. In some
cases the most accessible way to support and collaborate with them is
hosting their annual meetings. Other strong goal is to better use of the
facilities and resources of the city of Almería, helping it to evolve
into a technological city with international links beyond the current
successful agriculture and the touristic industries. We like the idea of
putting Almería in the map of the technological world. And after hosting
some of the most important conferences seems to be a moral due offering
to host the LibreOffice annual conference too.

The experience of the local team hosting other conferences:

- [PyConES 2016](http://2016.es.pycon.org/)

- [Akademy 2017](https://akademy.kde.org/2017)

- [Geocamp ES 2017](http://2017.geocamp.es/)

- [SuperSEC 2018](https://supersec.es/)

- [GUADEC 2018](https://2018.guadec.org/)

- [LibOConf 2019](http://liboconf.org)

### The city of Almería

![](Pictures/1000000000000140000000D5330F2D53265809D6.jpg)

[Almería](https://en.wikipedia.org/wiki/Almer%C3%ADa) is a sunny, lovely city on the Southeast coast of Spain. On the shores

of the Mediterranean sea, the city was founded in 955 by Abd-ar-Rahman
III, who ordered the construction of the famous [Alcazaba (the Citadel)](https://en.wikipedia.org/wiki/Alcazaba_of_Almer%C3%ADa),
one of the largest moorish castles in Europe. The modern city has grown
over the hills that surround the Alcazaba, staying true to its visual
and architectural tradition.

Almería is also the administrative capital of the homonymous province
and part of the [Autonomous Community of Andalusia](https://en.wikipedia.org/wiki/Andalusia).
The region is famous for its prolific production of vegetables, having
[over 100,000 acres of greenhouses in the Poniente province](https://en.wikipedia.org/wiki/Poniente_Almeriense#/media/File:Plastic_sea,_Almer%C3%ADa_Spain.jpg).
Almería also hosts scientific research facilities like the [Calar Alto Astronomy Observatory](http://www.caha.es/), the
[Almería Solar Platform](https://en.wikipedia.org/wiki/Plataforma_Solar_de_Almer%C3%ADa) and the [Experimental Station of Arid Zones](http://www.eeza.csic.es/en/).



![](Pictures/1000000000000140000000D5E708118C80E8687E.jpg)

The city has about 195,000 inhabitants who are welcoming to visitors and

always happy to enjoy the city\'s excellent weather with a vibrant day
and night life. Some famous Almerians are Nicolás Salmerón y Alonso, the
third president of the first Spanish Republic; as [Antonio de Torres](https://en.wikipedia.org/wiki/Antonio_de_Torres_Jurado),
considered the father of the modern Spanish guitar; Fidela Campiña, a
versatile opera singer; Carmen Pinteño, one of the best Almerian
painters; more recently, José Fernández Torres "Tomatito", one of the
best flamenco guitarists in the world; and David Bisbal, the famous pop
singer.

Almería is also famous for its [popularity with the filmmaking industry](https://www.turismodealmeria.org/wp-content/uploads/2016/04/land-of-film.pdf).
A good number of world famous films have been shot in the incomparable
and varied landscapes and towns of the province. Since the sixties,
Almería has attracted an enormous amount of films, commercial video and
videoclips. Recent films like Exodus and TV series like Game of Thrones
or Penny Dreadful have filmed here.



![](Pictures/1000000000000135000001E067EA0990353DB8F0.jpg)


[Aldous Huxley visited Almería in 1929](http://www.almediam.org/PDF/Huxley.pdf). He was so taken by Almería that he wrote a sonnet about it:



> Winds have no moving emblems here, bud scour
> A vacant darkness, un untempered light;
> No branches bend, never a tortured flower
> Shudders, root-weary, on the verge of flight;
> Winged future, withered past, no seeds nor leaves
> Attest those swift invisible feet: they run
> Free through a naked land, whose breast received
> All the fierce ardour of a naked sun.
> You have the Light for lover. Fortunate Earth!
> Conceive the fruit of his divine desire.
> But the dry dust is all she brings to birth,
> That child of clay by even celestial fire.
> Then come, soft rain and tender clouds, abate
> This shining love that has the force of hate.



Visiting Almería is a great opportunity to enjoy some tourism with
different landscapes and weathers, all less than 100 km away from the
city.

And if you believe being at the South of Spain can be very hot\... well,
maybe it is, but probably not as much you expect. Other cities in
Andalucía, far from the sea, can be really hot in summer with 40 ºC or
more and hot enough. But being at the seaside our temperatures are
always smoothed by the Mediterranean sea. Its weather is similar to
places like Málaga, Barcelona, Alicante or Mallorca, which receive
millions of tourists each year.

![](Pictures/10000000000003200000012257B8D20B40B93D4A.jpg)


Give yourself an opportunity to fall in love with Almería.



![](Pictures/10000201000003E800000230A75B187CD176E3E5.png)


### Proposer

The organization is lead by the non-for profit NGO association Club de
Cacharreo, id/VAT number: G04802534, located in the city of Almería
(Spain) and registered with the number 05905 at the Junta de Andalucía
NGO\'s registry. Club de Cacharreo. The association is ready for sending
and receiving payments, invoices, etc.

Other collaborators are:

- UNIA, the computer sciences student association at the University of Almería

- HackLab Almería, the technological informal community in the Almería

  city and province.

### The team

The organization team is lead by Ismael Olea, president of the Club de
Cacharreo association, and for decades known member in the opensource
community in Spain and Ibero-America. Ismael accumulates years of
experience not only in community involvement but in the organization of
opensource related activities and national or international conferences
since 1998. Ismael has experience as a team leader, relation and
sponsors contracting, etc.

As a link with the Almería University the professor Julián García
Donaire.

The team is completed by usual local collaborators:

- Victor Suarez Garcia, member of the HackLab Almería community and co-organizarer of El Hackatón 2014 and 2018 (Almería);

- Rubén Gómez Antolí, board member of Club de Cacharreo and co-organizer of the Akademy 2017 in Almería;

- Alejandro Pino España, from UNIA, CS students association;

- Juanjo Salvador Piedra, volunteer on several conferences and member of the HackLab Almería community.

![](Pictures/10000000000001F10000017522C02167AA069E4C.jpg)


### The venue

The proposed venue is the [University of Almería](https://ual.es/en/) campus. You can check the
campus localization at [OpenStreetMap](https://www.openstreetmap.org/way/29220363)
or [Google Maps](https://www.google.es/maps/place/University+of+Almeria/@36.8217122,-2.405678,548a,35y,359.71h,55.4t/data=!3m1!1e3!4m5!3m4!1s0x0:0x2a59668c430f3ff3!8m2!3d36.8293223!4d-2.4044609).

You can explore the campus through this [360º virtual visit](https://w3.ual.es/visitavirtual/).

### The university

![](Pictures/10000000000003CD0000015ACC4D2695489CA6EB.jpg)

Currently, UAL has 11.000 students, a 900 teachers staff, and offers
[31 official careers](https://www.ual.es/en/estudios).



It is a fairly new university, founded in 1993 so you can congratulate
us in our [25 Anniversary](http://www.25ual.es/). It
is almost completely hosted in one campus placed at about 6 Km from
Almería city, just a few meters from the sea, literally.

You can check the [UAL campus map](http://cms.ual.es/idc/groups/public/@serv/@infraestructura/documents/actividad/planodelcampus.pdf).

![](Pictures/1000000000000280000001E067AAC5853664EDBE.jpg)


![](Pictures/1000000000000280000001E0F56A1E98483F48B1.jpg)


![](Pictures/10000000000006EC00000531EBCD91956DC71972.jpg)


Places for conference activities

+-----------------------------------+-----------------------------------+
| ::: {.g-fr12}                     | [Auditorium](https://www. |
| ![](Pictures/10000000000002800000 | openstreetmap.org/way/37639082){. |
| 01E0E5175C913847CB9C.jpg){.calibr | calibre2}                         |
| e1}                               |                                   |
| :::                               | -   Capacity for 300 people.      |
|                                   |                                   |
|                                   | -   Projectors.                   |
|                                   |                                   |
|                                   | -   Wireless Internet connection. |
|                                   |                                   |
|                                   | -   Fully accessible with a       |
|                                   |     wheelchair.                   |
+-----------------------------------+-----------------------------------+
|                                   | [Aulario IV  building](https://www.openstreetmap.org/way/37639116) |
|                                   |                            |
|                                   |                                   |
|                                   | -   Conference room for 98        |
|                                   |     people.                       |
|                                   |                                   |
|                                   | -   Several classrooms (80m2).    |
|                                   |                                   |
|                                   | -   All equipped with projectors  |
|                                   |     and wireless Internet         |
|                                   |     connection.                   |
|                                   |                                   |
|                                   | -   Fully accessible with a       |
|                                   |     wheelchair.                   |
|                                   |                                   |
|                                   | -   A big hall for conference     |
|                                   |     registration, coffee breaks,  |
|                                   |     open space informal meetings  |
|                                   |     and stands.                   |
+-----------------------------------+-----------------------------------+
|                                   | [University cafeteria](https://www.openstreetmap.org/way/37639300) |
                          |
|                                   |                                   |
|                                   | -   Capacity for more than 300    |
|                                   |     people.                       |
|                                   |                                   |
|                                   | -   Price of menu to be           |
|                                   |     negotiated with the meal      |
|                                   |     service.                      |
|                                   |                                   |
|                                   | -   Fully accessible with a       |
|                                   |     wheelchair.                   |
|                                   |                                   |
|                                   | -   Wireless connection.          |
+-----------------------------------+-----------------------------------+
|                                   | [Childcare facilities](https://www.openstreetmap.org/way/37639423) |
|                                   | -   Opened from 07:00 am to 20:00 |
|                                   |     pm.                           |
|                                   |                                   |
|                                   | -   2 classrooms for 8 children   |
|                                   |     (0 to 1 year old).            |
|                                   |                                   |
|                                   | -   3 classrooms for 13 children  |
|                                   |     (1 to 2 years old).           |
|                                   |                                   |
|                                   | -   4 classrooms for 15 children  |
|                                   |     (2 to 3 years old).           |
+-----------------------------------+-----------------------------------+

The avaiability of rooms as described flexibilizes the tracks
organization. With Auditorium and the conference room we can have a two
tracks setting. If desired the classrooms can be used as conference room
too or as hacking or BoF spaces.

### Conference duration

Managing the reservation with sufficient advance we could choose 3, 4 or
more days, extending the time for BoF, meetings or hackmeetings.

### Video recording and streaming services

The University of Almería offers both services with some restrictions
related with buildings. Particularly the Auditorium can support both.
For other spaces it should be consulted with the service operators.

### Communications with the campus

![](Pictures/1000000000000140000000F0A308C3EA1B97CB0C.jpg)


#### Conference direct bus

For the main days the conference will setup a direct bus from CIVITAS to
the campus. We\'ll announce the precise time a few days before the
conference. The bus will make stops at:

- at [CIVITAS residence](https://goo.gl/maps/ZG5H9jTbAmL2);

- at [the intermodal station](https://goo.gl/maps/pJn6U7swo8C2) (the bus/train central station).

#### Regular bus

Several regular bus lines connect the city with the campus. Depending
from/to which part of the city you travel you\'ll need a different line.
In doubt please ask the organization team.

You should know:

- [Line 18](http://m.surbus.com/lineas/linea/18) operates every day of the week

- [Line 12](http://m.surbus.com/lineas/linea/12) stops at campus just on workdays

- [Line 11](http://m.surbus.com/lineas/linea/11)stops at campus just on workdays

You can check the waiting time for each bus at [the precise bus stop](http://m.surbus.com/tiempo-espera/paradas).

For working days:

- When going from CIVITAS to campus you\'ll probably would like to take L12 bus at [bus stop 71](https://www.openstreetmap.org/node/469474253) ([check waiting time](http://m.surbus.com/tiempo-espera/parada/71)).

- When back to CIVITAS from campus you\'ll want to take the L11 bus and get down at [bus stop 56](https://www.openstreetmap.org/node/469474241) ([check waiting time](http://m.surbus.com/tiempo-espera/parada/56)).

- For going and coming from the campus it's the same [bus stop 144](https://www.openstreetmap.org/node/974730957) ([check waiting times](http://m.surbus.com/tiempo-espera/parada/144)).

#### Taxi

A taxi ride from the city to campus costs about 6 €. You can ask one at
the [Pidetaxi web application](https://pidetaxi.es/book_taxi). To go to
campus just ask driver to go to the main entrance of the university.

You can ask for a taxi using Whatsapp using in English. [More info](http://www.radiotaxialmeria.com/peticion-de-taxi-950-22-22-22/).

#### Bike

There are a bike track from the city to the campus. At the campus
you\'ll find bike lockers. Bring your own padlock. You can hire a bike
for the conference.

![](Pictures/1000000000000627000003EFD8222BA2AA7FDC40.jpg)


#### Car

It's plenty of parking space too. We recommend [the one just at the main entrance](https://www.openstreetmap.org/way/36406072).

### ATM

- There is an [ATM at campus](https://www.openstreetmap.org/node/999522025) just in front the main auditorium.

- Near CIVITAS you\'ll find at least an ATM at [C/ Calzada de Castro, 50](https://www.openstreetmap.org/node/4497678840).



![](Pictures/10000000000001F100000175C0AB88AD37B2ADB4.jpg)


### Internet connectivity

There is a campus wide WiFi network. Network access is compatible with
the [Eduroam network](https://www.eduroam.org/) accounts. Conference will have our own VLAN WiFi access activated in the
places reserved for us.

The campus has 3G/4G coverage from the main Spanish carriers.

### On-site catering

Meals will be held at the mentioned [campus cafeteria](https://www.openstreetmap.org/way/37639300).
It can host more than 300 persons at same time and is able to attend
several hundreds in a normal day. It offers healthy menus following the
Mediterranean diet including salad, appetizer, main dish, dessert/fresh
fruit, bread and drinks. [Gazpacho](https://commons.wikimedia.org/wiki/Category:Gazpacho) will be a conference menu\'s special feature.



![](Pictures/10000000000001F10000017569B52A7894AE7954.jpg)

In working days there are other smaller canteens but they all close at 14:00.

### Kindergarten service

We are offering a kindergarten/nursery service for our guests. Please
reach to organization team for details and reservations. The service is
provided in a specialized building in the campus. It does not include
meals.

### Accesibility

Law in Spain obligues public buildings to be accessible so all the
spaces in the university campus in this document are accessible.

### Map

Here it is a general map of the University of Almería campus:

![](Pictures/10000201000002D2000001416C4D1121809BE541.png)



### Rooms for private meetings

For any kind of private or restricted meetings we can reserve a meeting
room at the Civitas residence or a classroom at the university.

### Main lodging place: Civitas dormitory

The main option and our recommended accommodation for this year is
[Residencia Civitas](http://www.residenciacivitas.com/en/) (view the [promotional video](http://www.youtube.com/watch?v=v0PAAprhc_U)).

![](Pictures/10000000000002C900000190888C1DD46E5FDE6D.jpg)


Civitas is a students dormitory with enough space for all LibOCon
attendees and with great [facilities](http://www.residenciacivitas.com/en/facilities-civitas-residence/).
It is about 5 minutes away from the bus and train station. It is in a
great location because of the transport connections to the university
and also with the center of Almería.

#### Price

LibOCon attendes can have a special price for Civitas dormitory:

![](Pictures/10000000000003E8000002EE09FEE281439DCD81.jpg)


Single room

- 32.75 € per night, breakfast included

- 42.10 € per night, breakfast and evening dinner included



Twin room

![](Pictures/10000000000003E8000002EE337F6AF3AADA6744.jpg)

- !46.75 € per night, breakfast included
- 56.10 € per night, breakfast and evening dinner included

When making your reservation, remember that there is dinner available at
some of the planned evening events.

#### Common rooms

Each floor has a common room with where you can relax and chat with
other attendees.

Address: [Calle Fernán Caballero, 1, Almería (Spain).](https://www.openstreetmap.org/way/202272064)

#### How to arrive

##### From the airport

- By bus

  1. The bus stop at the airport is the number 188 (you can check the [wait time](http://m.surbus.com/tiempo-espera/parada/188)).

  2. You need [to take Line 30](http://www.surbus.com/inicio.aspx?cat=0&id=30) (1,05 €). Line 30 connects the city, the airport and the suburbs of Retamar (end of line). Since the bus stop is the same in both directions asks bus driver it\'s going back to the city. If not, you can choose either get the bus and pay again at end of line or wait till the bus take the route back from the suburbs.

  3. Get off at "Estación Intermodal", bus stop \#292 (you can [check the wait time](http://m.surbus.com/tiempo-espera/parada/292)). This is just [outside the us and train station](https://commons.wikimedia.org/wiki/Category:Almer%C3%ADa_train_station#/media/File:Antigua_estaci%C3%B3n_de_ferrocarril_de_Almer%C3%ADa.JPG). 

  4. Walk 5 minutes to Civitas (check the [OpenStreetMap route](http://www.openstreetmap.org/directions?engine=mapzen_foot&route=36.83520%2C-2.45601%3B36.83752%2C-2.44828#map=17/36.83641/-2.45216)).

- By taxi, you can use the PideTaxi application:

  - [web app](https://pidetaxi.es/book_taxi)

  - [smartphone app](https://pidetaxi.es/apps)

If you have a problem to choose the correct destination use the
alternative «Nuestra Señora de Montserrat, 102, Almería».

##### From CIVITAS to LibOCon

![](Pictures/1000000000000140000000F0A308C3EA1B97CB0C.jpg)


For the main days the conference will setup a direct bus from CIVITAS to
the campus. We\'ll announce the precise time a few days before the
conference.

The bus will make stops at:

- at [CIVITAS residence](https://goo.gl/maps/ZG5H9jTbAmL2);

- at [the intermodal station](https://goo.gl/maps/pJn6U7swo8C2) (the bus/train central station).

#### Other accommodation alternatives

One of the Almería city advantages is not being a massive touristic
place (think Barcelona, Málaga or Palma de Mallorca, visited by millions
each year) but provides a good hosting offering with several qualities
until 4 stars. Consider than 4 stars hotels in Spain can be quality
similar to 5 stars ones in other countries.

You can use platforms such as [Trabber](http://trabber.com/), Booking, TripAdvisor
or Trivago to search for the accommodation you like.

Here is a list for your reference

##### 4 stars hotels

+-----------------------------------+-----------------------------------+
| ::: {.g-fr22}                     | [Nuevo                           |
| ![](Pictures/10000000000005DC0000 | Torreluz](http://en.nuevot |
| 02814ABF2020BBCDFD7C.jpg){.calibr | orreluz.com/),         |
| e1}                               | [Directions](https://www. |
| :::                               | openstreetmap.org/node/1711184296 |
|                                   | )                      |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr23}                     | [NH Ciudad de                    |
| ![](Pictures/10000000000002400000 | Almería](http://www.nh-hot |
| 02EE839C810DA4BE852C.jpg){.calibr | els.com/hotels/almeria){.calibre2 |
| e1}                               | },                                |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/way/27152916){. |
|                                   | calibre2}                         |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr24}                     | [AC Hotel Almería                |
| ![](Pictures/10000000000008000000 | Marriot](http://www.marrio |
| 044C0C6FC989E3684153.jpg){.calibr | tt.com/hotels/travel/leial-ac-hot |
| e1}                               | el-almeria/),          |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/node/3546471699 |
|                                   | )                      |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr25}                     | [Hotel                           |
| ![](Pictures/10000000000002260000 | Catedral](http://www.hotel |
| 019C724F0BD0F2F2FF65.jpg){.calibr | catedral.net/),        |
| e1}                               | [Directions](https://www. |
| :::                               | openstreetmap.org/node/2697637577 |
|                                   | )                      |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr26}                     | [Elba Almería                    |
| ![](Pictures/10000000000004000000 | Hotel](https://www.hoteles |
| 02AA00F6AB8B144628FA.jpg){.calibr | elba.com/en/hotel-elba-almeria-bu |
| e1}                               | siness-convention),    |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/node/442706297) |
|                                   |                        |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr27}                     | [Tryp Indalo                     |
| ![](Pictures/100000000000016F0000 | Almería](https://www.melia |
| 00F5948897A0E5F5CE06.jpg){.calibr | .com/en/hotels/spain/almeria/tryp |
| e1}                               | -indalo-almeria-hotel/){.calibre2 |
| :::                               | },                                |
|                                   | [Directions](https://www. |
|                                   | openstreetmap.org/node/442686279) |
|                                   |                        |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr27}                     | [Hotel Sercotel Gran             |
| ![](Pictures/10000000000004000000 | Fama](http://en.hotelgranf |
| 02ABFA34CF3A5CF5415E.jpg){.calibr | ama.com/),             |
| e1}                               | [Directions](https://www. |
| :::                               | openstreetmap.org/node/2134900027 |
|                                   | )                      |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr28}                     | [Avenida Hotel                   |
| ![](Pictures/10000000000002580000 | Almería](http://www.avenid |
| 015EFD6599341216905D.jpg){.calibr | ahotelalmeria.com/en/) |
| e1}                               | ,                                 |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/way/505032860){ |
|                                   | .calibre2}                        |
+-----------------------------------+-----------------------------------+

##### 3 stars hotels

+-----------------------------------+-----------------------------------+
| ::: {.g-fr29}                     | [Hotel                           |
| ![](Pictures/10000000000001770000 | Costasol](http://www.hotel |
| 01F46D97FCC5EAB46068.jpg){.calibr | costasol.com/en/)      |
| e1}                               |                                   |
| :::                               |                                   |
+-----------------------------------+-----------------------------------+

[]{#anchor034 .calibre2}2 stars hotels

+-----------------------------------+-----------------------------------+
| ::: {.g-fr26}                     | [Torreluz                        |
| ![](Pictures/10000000000003200000 | Centro](http://www.torrelu |
| 021504BD6C9E734F2A32.jpg){.calibr | z.com/es/alojamiento-torreluz-cen |
| e1}                               | tro),                  |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/node/1730264907 |
|                                   | )                      |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr29}                     | [Embajador](http://www.ho |
| ![](Pictures/10000000000001E00000 | telembajador.es/en/),  |
| 0280CEA0F21E1110CFC0.jpg){.calibr | [Directions](https://www. |
| e1}                               | openstreetmap.org/way/230511112){ |
| :::                               | .calibre2}                        |
+-----------------------------------+-----------------------------------+

[]{#anchor035 .calibre2}Aparthotels

+-----------------------------------+-----------------------------------+
| ::: {.g-fr26}                     | [Aparthotels                     |
| ![](Pictures/10000000000003E80000 | Torreluz](http://www.torre |
| 029B826D561522A34A70.jpg){.calibr | luz.com/es/alojamiento-apartament |
| e1}                               | os-torreluz),          |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/node/4950780938 |
|                                   | )                      |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr26}                     | [16:9 Suites                     |
| ![](Pictures/10000000000002260000 | Ciudad](http://dieciseisno |
| 016E1F174EA23165C79F.jpg){.calibr | venossuites.com/suites){.calibre2 |
| e1}                               | },                                |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/way/458511247){ |
|                                   | .calibre2}                        |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr30}                     | [16:9 Suites                     |
| ![](Pictures/10000000000001F40000 | Playa](http://dieciseisnov |
| 01EAA331DA207007AAFD.jpg){.calibr | enossuites.com/apartamentos/169-s |
| e1}                               | uites-playa),          |
| :::                               | [Directions](https://www. |
|                                   | openstreetmap.org/way/44512016){. |
|                                   | calibre2}                         |
+-----------------------------------+-----------------------------------+

##### Hostales

From Wikipedia entry: «A [hostal](https://en.wikipedia.org/wiki/Hostal) is a type of [lodging](https://en.wikipedia.org/wiki/Lodging) found mostly in
[Spain](https://en.wikipedia.org/wiki/Spain) and [Hispanic America](https://en.wikipedia.org/wiki/Hispanic_America).
Hostales tend to be cheaper than
[hotels](https://en.wikipedia.org/wiki/Hotel). They
normally have a bar, restaurant or cafeteria where drinks and food are
sold to guests and locals alike.»

There is a great offer of hostales along the city.

### Proposed evening social activities

First we introduced you to the diversity and alternatives for doing
tourism in Almería. Here we propose a set of group activities, cultural
and social we hope you'll enjoy and love.

All the outdoors activities are thought to be done at evening, probably
from 18:30, so our guests would have time to go back the hotel, get a
shower and be ready to go out when the evening starts to refresh.

#### Welcome party

![](Pictures/100000000000027C000001A9AB7E5C45E9786E0A.jpg)


A welcome party, outdoors in the dormitory yard, to have some drinks and
meet again our colleagues.

- Place: Civitas Residence

- Time: about 18:30

#### City downtown visit

![](Pictures/100000000000032000000218D4476D33C091BC3B.jpg)


A guided visit to the beautiful XIX bourgeois downtown of Almería city,
plus the restored [air raid shelters](https://en.wikipedia.org/wiki/Almer%C3%ADa_air_raid_shelters)
built in the Spanish Civil War. The visit would end with a little party
at the city hall terrace, with lovely views to the Alcazaba castle.

![](Pictures/1000000000000258000001900480CF6065B5C904.jpg)


![](Pictures/1000000000000800000006002F5B5A9E1A258255.jpg)

#### Flamenco show

Almería has a long history and tradition in Flamenco. The «[cante de las minas](https://es.wikipedia.org/wiki/Minera_(cante_flamenco))»
style born in the XIX when flourished the mining activities. Some of the
most famous flamenco guitar players comes from the city and here is too
[El Taranto](http://www.eltaranto.com/), the oldest peña flamenca (flamenco club) of Spain.

![](Pictures/10000000000002260000016EB2DD52B4CA473967.jpg)




We propose a singing and dancing flamenco show, about 1 hour of
duration.

#### Spaghetti Western Show

Did we tell you about movies being made in Almería? Probably yes,
several times ? But yes, the province has an [spectatular background on cinema](https://en.wikipedia.org/wiki/List_of_films_shot_in_Almer%C3%ADa).


![](Pictures/100002010000031A0000018E4FD89F3C8CD5C426.png)

 Dozens of very well known movies, TV series, TV spots or music videos

had been shoot full or partially here. It has been so significative that
we keep cinema stages built in the 70s\... available for you for awesome
shows of cowboy\'s western reenactment by professional actors.


![](Pictures/10000000000003C200000273DA9C2C44795EA4F6.jpg)



![](Pictures/10000000000002B80000020ADCFCBD106E3D5A29.jpg)


![](Pictures/10000000000001E000000140E6DE78CEB875803F.jpg)


So we propose an evening visit to a set of cinema stages at [Tabernas
desert](https://en.wikipedia.org/wiki/Tabernas_Desert),
about 30 km from the city, to attend to two [Spaguetti Western inspired
shows](https://www.oasysparquetematico.com/en/western-town/):


![](Pictures/1000000000000683000009C483EE8EF2D1F6BA11.jpg)



- a cowboys shooting show, imitation of all that old movies;

- a [can-can](https://en.wikipedia.org/wiki/Can-can) dancing show at the saloon;

- and, finally, a barbecue with the nice weather of the beginning of the night.

We warn you: take care of Sentencia, a badass gunman who used to walk
around the town.


![](Pictures/10000000000001E00000014082D237FFD4415C09.jpg)

![](Pictures/100000000000025800000192778657CA77556B5F.jpg)}

![](Pictures/100000000000019000000126FF35AF6A8A094CC0.jpg)

#### Visit to the Museum of Almería

Since the times when the Belgian engineer [Luis
Siret](https://en.wikipedia.org/wiki/Luis_Siret)
accidentally discovered his first archaeological finding when building
the first railway in the province the Archeological studies in Almería
had discovered significatives traces from the past through several
cultures: Iberian, Phoenician, Roman, Arabic and Christian. One of the
most important findings are the related with [Los Millares
culture](https://en.wikipedia.org/wiki/Los_Millares), one of the very first urban cities and human civilization starting
points in Europe.

![](Pictures/1000000000000190000002585B2239AE06CAE583.jpg)


![](Pictures/10000000000009C400000683D009FDC2F66AC82E.jpg)

![](Pictures/100000000000033B000002037A4EF8498DB98DBF.jpg)

So we propose a theatralized visit to the [Museum of
Almería](http://www.museosdeandalucia.es/cultura/museos/MAL/index.jsp?redirect=S2_1.jsp&lng=en) inspired by Luis Siret himself to dive into the dawn of the civilization
in Europe.

![](Pictures/10000000000009C400000753E73B9832369B154D.jpg)




After the visit will have an informal catering or picnic outdoors in the
museum\'s yard.

#### Alcazaba visit

The [Alcazaba of
Almería](https://en.wikipedia.org/wiki/Alcazaba_of_Almer%C3%ADa)
is a fortified complex in Almería. The word
[alcazaba](https://en.wikipedia.org/wiki/Alcazaba), from the Arabic word al-qasbah, signifies a walled-fortification in a
city. It is a thousand years old and it\'s building was the starting
point of what is now the city of Almería


![](Pictures/10000000000002A8000001C6868DF776F6DD47B5.jpg)

![](Pictures/1000000000000640000004B0270209D44447241A.jpg)




The Alcazaba has been used to film [Conan the
Barbarian](https://en.wikipedia.org/wiki/Conan_the_Barbarian_(1982_film)),
[Indiana Jones and the Last
Crusade](https://en.wikipedia.org/wiki/Indiana_Jones_and_the_Last_Crusade),
and [Never Say Never
Again](https://en.wikipedia.org/wiki/Never_Say_Never_Again)
as well as the syndicated TV series [Queen of
Swords](https://en.wikipedia.org/wiki/Queen_of_Swords_(TV_series)) used the inner courtyard and gardens.used the inner courtyard and gardens.


![](Pictures/1000000000000438000002A37A08BF72F574AF54.jpg)


You can check for more [pictures of the Alcazaba published in

Wikicommons](https://commons.wikimedia.org/wiki/Category:Alcazaba_de_Almer%C3%ADa).


![](Pictures/1000000000000320000001FFEB76ECD211A3E1B9.jpg)

We propose a fun inmersion in history with a set of related activities:

- interactive theatrical visit to the castle complex;

- a medieval combat reenactment between Andalusian defenders against, wait for it, the Vikings! [inspired by historical facts](https://en.wikipedia.org/wiki/Viking_raid_on_Seville);


![](Pictures/1000000000000400000002AFBE662E6B957D2DD5.jpg)


![](Pictures/1000000000000500000002D024161B3AFFB9F4E2.jpg)

![](Pictures/10000000000004B0000002A364814E7A22BDA3BF.jpg)


#### Tapas route

Extracting from the Wikipedia entry for
[tapas](https://en.wikipedia.org/wiki/Tapas):

![](Pictures/1000000000000140000000DEC7508DC0D86F574C.jpg)

> A tapa, in Spanish cuisine, is an appetizer, or snack. It may be cold (such as mixed olives and cheese) or  hot (such as chopitos, which are battered, fried baby squid). In select bars in Spain, tapas have evolved into an entire, sophisticated cuisine.

![](Pictures/1000000000000140000000CBE8AF6059DC401569.jpg)

Almería locals and visitors love to eat tapas. When meeting to eat with
friends we usually go to one or several bars and eat several tapas. In
Almería each tapa includes a beverage (usually wine, beer or grape
juice) and an appetizer. Obviously you can drink any other beverage but
it\'s not included in the standard price. Do you can have a full lunch
with tapas? Yes, sure. The rule of thumb is you can have a meal with
three tapas. Going for tapas is really popular for dinner.


![](Pictures/1000000000000320000002134CA303337F23DC83.jpg)


And we are so proud about our tapas experience we want to share it with
you. In this case it would be a «distributed event»: since there is no
tapas big enough to hold all our guests we\'ll propose a list of nice
bares available in a zone of the downtown so attendees could organize
into groups of, say, 10 persons and distribute themselves spontaneously
along the listed bars to enjoy the Almería tapas experience: a little
walking, a light and enjoyable dinner and meeting in the streets with
friends here and there.

And you\'ll have the support of the local team who will be around
offering any help you could need.

From a previous conference here you have a map with a first [list of
«bares de
tapas»](https://www.google.com/maps/d/viewer?mid=1Qm5_h5uTqriVSYBHS_SP3NVgjHc&ll=36.84059757270377%2C-2.4157657280029525&z=13)
we will use. This is another great great list of tapa\'s bars created
for the past [10th Almería\'s Tapas
Route](http://www.weeky.es/x-ruta-de-tapas-por-almeria-2017-ashal/)
(in Spanish, sorry).


![](Pictures/1000000000000140000000DEC7508DC0D86F574C.jpg)

![](Pictures/10000000000009C4000007537DA7FB822ABA870E.jpg)

![](Pictures/1000000000000280000001E00651D82A5BB70DEF.jpg)

![](Pictures/1000000000000280000001E0E0D415917F32EB8A.jpg)

![](Pictures/1000000000000140000000F07404B10257FC3926.jpg)

![](Pictures/1000000000000140000000F065439ACF2B765704.jpg)

![](Pictures/1000000000000140000000F03E07389F5A82DC75.jpg)

![](Pictures/1000000000000140000000D621D460932AFC306D.jpg)

![](Pictures/1000000000000280000001A824968C3B1D5E0CCE.jpg)

![](Pictures/1000000000000140000000D5769FDADE9E7A0AD2.jpg)

![](Pictures/1000000000000117000000F0B649255A1D20AA98.jpg)

![](Pictures/1000000000000140000000D50B67DF19D4FA3DC7.jpg)

![](Pictures/1000000000000140000000D51105F12F6BA5CF78.jpg)

![](Pictures/1000000000000140000000D5A69206A1BA9FDA07.jpg)

![](Pictures/10000000000003E80000029BF2E8350593CF4B32.jpg)

![](Pictures/10000000000004FC00000362BEDD22D286A82903.jpg)

![](Pictures/10000000000009C4000006834C9CD72B7B6D69C0.jpg)

![](Pictures/1000000000000280000001E08316FBB5C817B60A.jpg)

### Travelling to Almería

#### Flying to Almería Airport (LEI)




![](Pictures/1000000000000140000000F072CEEF03EE84B629.jpg)


[Almería Airport](http://www.aena.es/en/almeria-airport/index.html) has frequent connections to Madrid (Iberia) and almost daily ones to
Barcelona (Vueling). It also has flights to other European airports such
as London--Gatwick, Amsterdam, Brussels, etc.

At their website you can [check all
destinations](http://www.aena.es/en/almeria-airport/destinations.html)
and
[airlines](http://www.aena.es/en/almeria-airport/airlines.html).

Some considerations: in Summer season there are many direct connections
to several European cities, specially by low cost companies. To plan
your travel we suggest to check those alternatives since could save you
a significant amount of money or time. Please double check the airport
[destinations](http://www.aena.es/en/almeria-airport/destinations.html)
and
[airlines](http://www.aena.es/en/almeria-airport/airlines.html).

[Airport is
located](https://www.openstreetmap.org/way/37923960)
at 9 Km from the city and about 5 minute by car from the university
campus.

Once you are at the airport you can go to the city:

By bus:

![](Pictures/1000000000000140000000F0A308C3EA1B97CB0C.jpg)


1. The bus stop at the airport is the \#188 (you can check the [wait time](http://m.surbus.com/tiempo-espera/parada/188)).

2. You need [to take Line 30](http://www.surbus.com/inicio.aspx?cat=0&id=30) (1,05 €).

3. Line 30 connects the city, the airport and the suburbs of Retamar (end of line). Since the bus stop is the same in both directions asks bus driver it\'s going back to the city. If not, you can choose either get the bus and pay again at end of line or wait till the bus take the route back from the suburbs.

4. Get off at \"Estación Intermodal\", bus stop \#292 (you can [check the wait time](http://m.surbus.com/tiempo-espera/parada/292)). This is just [outside the bus and train station](https://commons.wikimedia.org/wiki/Category:Almer%C3%ADa_train_station#/media/File:Antigua_estaci%C3%B3n_de_ferrocarril_de_Almer%C3%ADa.JPG).

Or by taxi, you can use the PideTaxi application:

- [web app](https://pidetaxi.es/book_taxi)

- [smartphone app](https://pidetaxi.es/apps)

price:

- Estimated price from airport to city center (or CIVITAS): 20€

- Estimated price from airport to university campus: 15€

#### Flying to Málaga Airport (AGP)

[Málaga
airport](http://www.aena.es/en/malaga-airport/index.html)
is the fourth busiest in Spain and thus has flights to most of major
European airports and some non-European ones (Montreal, New York,
Casablanca, Tel Aviv).

You can check
[destinations](http://www.aena.es/en/malaga-airport/airport-destinations.html)
and
[airlines](http://www.aena.es/en/malaga-airport/airlines.html).


![](Pictures/1000020100000275000002F0B258C22AE01EB925.png)


From Málaga Airport to Almería:

- Direct bus: there\'s two daily direct buses operated by ALSA, a non stop one that takes around 3h and  one with two stops that takes 3.5 hours. Probably the best option from Málaga to Almería.

  - Price: Around 20€ one way

  - Málaga Airport - Almería: 14:15 and 15:30

  - Almería - Málaga Airport: 9:30 and 15:30

  - You can get a ticket by using the [ALSA web page](https://www.alsa.es/) and filling in Almeria as destination.

- By bus through Málaga: there\'s a bus service available that links Málaga Airport with Málaga, and it stops both at the main bus station and train station of Málaga. There are a few others daily ALSA buses from Málaga to Almería, taking between 3 and 5 hours depending on the intermediate stops. You can buy your tickets at the [ALSA website](https://www.alsa.es/).

In any case your stop in Almería is the [Estación intermodal](https://www.openstreetmap.org/way/27152911) which is 10 minutes walking from Civitas residence. There is a taxi stop at the Intermodal too.

- Hiring a car: check the [hiring car services at the Málaga

  airport](http://www.aena.es/en/malaga-airport/car-rental.html).

- Car-pooling: there are several people that offer car pool rides between different points of Málaga (some including the airport) toAlmería. [BlaBlaCar](https://www.blablacar.es/) is one of the companies that offers such a service. ![](Pictures/1000000000000140000000F053F48D5D3FAAECAC.jpg)

- By taxi: this may sound crazy, but there are several companies offering transfer between the Málaga Airport and Almería ([185€](http://malagaradiotaxi.es/tarifas.html), [220€](http://www.taxis-malaga.net/precios.php)) so if you pool with 4 people, it is not that expensive and travel time is around 2 hours.

#### By road

![](Pictures/10000201000001D30000018A288F2F4A32723D9E.png)


Almería has a good land communication with all the Mediterranean coast
by the [A-7
highway](https://en.wikipedia.org/wiki/Autov%C3%ADa_A-7),
which join up to France frontier in La Jonquera and, on the other side,
ends at Algeciras.

With Spanish interior zones, Almería is connected by the
[A-92](https://en.wikipedia.org/wiki/Autov%C3%ADa_A-92),
another highway that connects with the rest of Andalusia and Madrid,
Spain capital.

#### By train

![](Pictures/1000000000000280000001E0BFC7E90C8A2BA58A.jpg)


There are some lines that connect Almería with Madrid and other cities,
such as Granada, Seville, Linares and Antequera. The most important
option by train is from Madrid. Keep in mind train travel Madrid-Almería
takes no less than 6 hours.

More information at [Renfe rail transport
company](http://www.renfe.com/EN/viajeros/index.html)
website.

#### By ship

There are [some ship
lines](http://www.apalmeria.com/index.php?option=com_content&id=95)
that connect Almería with Africa: Melilla, Oran (Algeria) and Nador
(Morocco) with daily or regular ships and affordable prices. The
passengers terminal is in the seaside of the city, less than 1 km from
downtown.

### Mobile/cell phone in Spain

You\'ll find the city of Almería covered with 4G/3G connectivity. If you
have doubts about frequencies and compatibilities you can [check this
report](https://wiki.bandaancha.st/Frecuencias_telefon%C3%ADa_m%C3%B3vil)
(in Spanish, sorry).

If you own a cell phone line acquired inside the [European Economic
Area](https://en.wikipedia.org/wiki/European_Economic_Area)
then you\'ll enjoy [free
roaming](https://en.wikipedia.org/wiki/European_Union_roaming_regulations#Territorial_extent).

If you want to buy a pre-paid Spanish cell line there options starting
from 5€. You\'ll be required by law to identify yourself with a legal
document (passport, i.e.) when buying a cell line in Spain.

### Electric plugs

Probably you\'ll need some electricity, we guess. You can learn about
details of the [electric plug system in
Spain](https://en.wikipedia.org/wiki/Mains_electricity_by_country#Table_of_mains_voltages,_frequencies,_and_plugs).
In sort: 230 V and 50 Hz.

Plug types:

+-----------------------------------+-----------------------------------+
| [Type                            | [[ype                            |
| C](https://en.wikipedia.or | F]]https://en.wikipedia.or |
| g/wiki/AC_power_plugs_and_sockets | g/wiki/AC_power_plugs_and_sockets |
| #CEE_7/16_Alternative_II_){.calib | #CEE_7/3_socket_and_CEE_7/4_plug_ |
| re2}                              | (German_)              |
+-----------------------------------+-----------------------------------+
| ::: {.g-fr69}                     | ::: {.g-fr70}                     |
| ![](Pictures/100000000000014A0000 | ![](Pictures/100000000000014A0000 |
| 00BAC1C40D058A2FF4EF.jpg){.calibr | 008E4E6119F6514C831B.png){.calibr |
| e4}                               | e4}                               |
| :::                               | :::                               |
+-----------------------------------+-----------------------------------+

### Weather

The expected weather is hot. Consider hot summer clothes. Fortunately
the venue and residence has conditioned air so you\'ll don\'t suffer. If
you have clear skin consider seriously carry
[sunscreen](https://www.youtube.com/watch?v=xkgbWGBmgN8)
and maybe to wear a cap. It\'s good idea to keep hydrated. At the
conference we\'ll provide fresh water machines.

The probability of rain those days is almost zero. You can check the
weather prediction at the [National Meteorological
Agency](http://www.aemet.es/en/eltiempo/prediccion/municipios/almeria-id04013).

Bring swimsuit ;-)

### Getting an Spanish visa

First all you should know you are travelling to the [Schengen
Area](https://en.wikipedia.org/wiki/Schengen_Area)
which Spain is part of. The reference information for traveling to Spain
is on the [Ministry of Foreign Affairs
website](http://www.exteriores.gob.es/Portal/en/Paginas/inicio.aspx),
on the [information for
foreigners](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/InformacionParaExtranjeros/Paginas/RequisitosDeEntrada.aspx)
page.

You can check if you require a visa in [this
list](https://ec.europa.eu/home-affairs/sites/homeaffairs/files/what-we-do/policies/borders-and-visas/visa-policy/apply_for_a_visa/docs/visa_lists_en.pdf)
(PDF).

If you are required to have a visa, please read carefully the [entry
requirements for
foreigners](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/InformacionParaExtranjeros/Paginas/RequisitosDeEntrada.aspx).



Citing:

> At any event, entry may be refused (even with a valid passport and/or
> visa) at police checks in the following cases:
> 
> Journeys of a professional, political, scientific, sporting or religious
> nature or undertaken for other reasons

The presentation of any of the following documents may be required:

1. Invitation from a company or an institution to take part in meetings, conferences, etc., of a commercial, industrial or similar nature.

2. Document proving the existence of commercial, industrial relations, etc.

3. Entry tickets to trade fairs, congresses, conventions, etc.

4. Invitations, tickets, bookings or programmes indicating, as far as possible, the name of the host organisation and the duration of the stay, or any other document indicating the purpose of the visit.

Consider too:

1. A supporting document from the establishment providing accommodation

2. IMPORTANT: Under no circumstances shall the letter of invitation replace the foreigner\'s accreditation of all other entry requirements.

3. A return or round-trip ticket.

In order to substantiate economic means, the provisions of Order
PRE/1282/2007, of 10 May, on economic means, shall be taken into
account, which provides that foreigners must demonstrate that they have
sufficient means of support available in order to enter Spain. The
minimum amount that must be substantiated is € 73.59 per person per day,
with a minimum of € 661.50 or its legal equivalent in foreign currency.

The visa fee is usually 60.00 EUR.

#### How demonstrate you have funds enough

The way to do is showing their availability one way or other:

- full cash

- certified checks

- travel checks

- payment letters

- credit cards, with a bank account statement up to date

#### Travel health insurance

You need to contract an [Schengen Visa Travel
Insurance](https://duckduckgo.com/?q=%22Schengen+Visa+Travel+Insurance%22&t=hf&ia=web)
with 30,000€ minimum coverage.

[List of Schengen approved insurance companies
2017](https://eeas.europa.eu/sites/eeas/files/schengen_approved_insurance_companies_2017_1.pdf).

#### Documentation for your travel

Required documents:

- approved visa document

- valid passport

- round trip flight tickets

- two passport photos

- Schengen travel insurance confirmation

- invitation letter

- conference ticket (if applicable)

- proof of accommodation

- proof of means to stay

Extra documents could help at the Spanish border:

- if employed: employment contract

- if self-employed: copy of the business license

- if student: proof of enrollment

#### Asking for an Spanish visa

What you are going to ask for is a [Uniform Schengen
Visa](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/InformacionParaExtranjeros/Paginas/VisadosUniformeSchengen.aspx).
Citing:

Short-stay visa requests should be made by submitting a printed copy of
the correctly completed [official
application](http://www.exteriores.gob.es/ContenidoReutilizable/InformacionParaExtranjeros/Documents/visadoSchengen_EN.pdf)
(original and copy), which can be downloaded from this web page or
obtained from the Diplomatic Missions or the Consular Offices of Spain
abroad.

The visa should be requested in-person at the [Diplomatic Mission or
Spanish Consular
Office](http://www.exteriores.gob.es/Portal/es/ServiciosAlCiudadano/InformacionParaExtranjeros/Documents/Lista%20de%20Representaciones%20para%20la%20expedici%C3%B3n%20del%20visado%20uniforme.pdf)
in the place where the applicant legally resides. If there is no
Diplomatic Mission or Spanish Consular Office in a specific country,
applications can be made at the Diplomatic Mission or Consular Office
that represents Spain in said country.

The established fee must be paid when presenting the visa application.
This will not be returned if the application is unsuccessful. The
necessary requirements, as well as cases of reduction or waiver of the
fee, should be checked at the Diplomatic Mission or Consular Office
where the visa is requested, as these may vary depending on the
applicant\'s reason for travel and country of origin.

The maximum period to process a short-stay visa request is 15 calendar
days from the date the application was made. This period may be extended
to a maximum of 30 calendar days in certain circumstances.
Exceptionally, in those specific cases where additional documentation is
needed, this period can be extended to 60 calendar days.

The visa issued must be collected in-person from the corresponding
Diplomatic Mission or Consular Office within a period of one month after
the notification of its issuance.

If the visa is denied, the applicant will be notified by means of a
standardised form which will state the reason for which the visa request
was unsuccessful. In this situation, a Contentious-Administrative appeal
can be lodged to the High Court of Justice (Tribunal Superior de
Justicia) of Madrid within a term of two months from the date of
notification. Appeals for reversal may be lodged with the Diplomatic
Mission or Consular Office within a term of one month from the date of
notification of the denial of visa.

- Visa request form: [in English](http://www.exteriores.gob.es/ContenidoReutilizable/InformacionParaExtranjeros/Documents/visadoSchengen_EN.pdf)
  or [in Spanish](http://www.exteriores.gob.es/Portal/es/ServiciosAlCiudadano/InformacionParaExtranjeros/Documents/visadoSchengen_ES.pdf).

- [List of Spanish embassies and consulates](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/Paginas/EmbajadasConsulados.aspx).

- In case you are interested (and could read Spanish) the procedure is
  regulated by the official [policy for foreigners in Spain](https://www.boe.es/buscar/act.php?id=BOE-A-2011-7703).

#### More information

You can find a lot of detailed information in websites like [Schengen Visa Information](https://www.schengenvisainfo.com/)
(non-government affiliated).

### Tourism in Almería


![](Pictures/1000000000000320000002464742F372E0469B60.jpg)


The province landscapes are extraordinaire. You can find three or more
different climates/ecosystems in an hour travel car.

Usually our visitants fall in love with the [Cabo de Gata-Níjar Natural Park](https://en.wikipedia.org/wiki/Cabo_de_Gata-N%C3%ADjar_Natural_Park),
a sea and land protected space said to be one of the best places in
Spain to dive.

When in Almería you can take the opportunity to spend a few days resting
here. But be sure to do your reservations soon enough since is tourist
high season.

Since probably it will be the first time you\'ll visit Almería and
theses parts of Spain we feel you could take the opportunity to travel
with your family.

![](Pictures/100000000000032000000258F6FA18B63EEC0F0D.jpg)

So they can have a parallel set of activities of active tourism both in
the city or in any of the beautiful landscapes, specially the [Cabo de Gata-Níjar Natural Park](https://en.wikipedia.org/wiki/Cabo_de_Gata-N%C3%ADjar_Natural_Park).
Indeed you could want to spent holidays time after the conferences with
your significant ones. An you\'ll find a lot of [companies offering outdoors activities](https://www.turismodealmeria.org/en/what-to-do/experiences/):

- tracking routes, both in park and other places in the province

- bus guided visits to beautiful places, both in park and other places in the province

- horse riding

- snorkeling in the natural park waters

- scuba diving baptisms in the natural park waters

- kayak routes in the natural park waters

  ![](Pictures/10000000000001900000010F8C0ECAD2C1738DEC.jpg)

- The [city of Almería has its own offers](https://www.turismodealmeria.org/en/travel-preparations/culture-and-traditions/):

- [The Alcazaba moorish castle](https://en.wikipedia.org/wiki/Alcazaba_of_Almer%C3%ADa) about 1000 years old

- [The Museum of Almería](https://en.wikipedia.org/wiki/Museum_of_Almer%C3%ADa)

- a beautiful XIX bourgeois downtown

- and the restored [air raid shelters](https://en.wikipedia.org/wiki/Almer%C3%ADa_air_raid_shelters) build in the Spanish Civil War

- and a lot of others [historic places and museums](https://www.turismodealmeria.org/en/travel-preparations/culture-and-traditions/#museums-and-exhibition-certres) to visit.

There is an [Almería Tourist Guide application](https://play.google.com/store/apps/details?id=com.segittur.almeria) for Android.

Very near the city (less than 30 Km far):

- visit to the [Los Millares Calcolithic archaeological site](https://en.wikipedia.org/wiki/Los_Millares), five thousand years old, one of the most important of the world for that age;

  ![](Pictures/1000000000000122000000D11A4418CF8E544478.jpg)

  

  

- attend to the [recreation and espectable of Western movies](http://fortbravooficial.com/) in real cinema stages used in world famous Spaguetti Western movies, like Sergio Leone and Clint Eastwood ones or TV shows like Dr. Who; 

- visit to the international research place [Almería Solar Platform](https://en.wikipedia.org/wiki/Plataforma_Solar_de_Almer%C3%ADa);

- visit to the [German-Spanish Astronomical Centre of Calar Alto](http://www.azimuthspain.com/en/calar-alto/).

A bit more far there are some nice destinations:

- the [Alpujarras](https://en.wikipedia.org/wiki/Alpujarras) zone has great places with exotic landscapes and towns with ancient origins, Moorish and sooner indeed;

- The marvellous [city of Granada](https://en.wikipedia.org/wiki/Granada) and province is less than two hour by car. Remember there is the [Alhambra](https://wiki.gnome.org/Alhambra), a World Heritage place [Washington Irving](https://en.wikipedia.org/wiki/Washington_Irving) felt in love and which inspired his famous [Tales of the Alhambra](https://en.wikipedia.org/wiki/Tales_of_the_Alhambra).

- and the first nudism hotel in Europa at the [4 stars Vera Playa Club Hotel](https://www.playasenator.com/hoteles/vera-playa-club-hotel/), if you like ![](Pictures/100002010000001000000010BF25F90F74CF96DA.png)

- [Málaga city](https://en.wikipedia.org/wiki/M%C3%A1laga) and province, a popular place for sun and beach tourism and a nice places like the [white towns](https://en.wikipedia.org/wiki/White_Towns_of_Andalusia).

![](Pictures/1000000000000320000002134CA303337F23DC83.jpg)

### W̶h̶e̶r̶e̶ How to eat in Almería

Well, as you can imagine we have restaurants and other meal shops here,
but what we really love is [to go for «tapas»](https://www.turismodealmeria.org/en/what-to-do/gastronomy/) ![](Pictures/100002010000001000000010BF25F90F74CF96DA.png)

![](Pictures/10000000000000FA0000015A27694B4F5CB6477D.jpg)


Almería locals and visitors love to eat tapas. When meeting to eat with
friends we typically go to one or several bars and eat several tapas. In
Almería each tapa includes a beverage (usually wine, beer or grape
juice) and an appetizer. Obviously you can drink any other beverage but
it\'s not included in the standard price. Do you can have a full lunch
with tapas? Yes, sure. The rule of thumb is you can have a meal with
three tapas. Going for tapas is especially popular for dinner.

So, remember, the ideal way to have your dinner in Almería is going to
tapas and as reference you can use these routes of tapas:

- [Ruta de tapas por Almería](http://www.andalucia.org/es/eventos/ruta-de-tapas-por-almeria/)

- [XI ruta de tapas Almería](http://www.weeky.es/xi-ruta-de-tapas-almeria-2018/)

- [Ruta de tapas por Almería](https://www.guiarepsol.com/es/comer/de-tapeo/ruta-de-tapas-por-almeria/)

### More touristic information

You can get more information on

- [Almería City Turism](http://www.turismodealmeria.org/en/)

- Andalucia.com

  ![](Pictures/100000000000020000000180C52A387F7D6358C8.jpg)

  

- [Municipal Tourist Office of Almería](http://www.andalucia.org/en/contact-us/almeria/almeria-3/)

You can follow other tourist news on:

- Twitter: [@almerialovers](https://twitter.com/almerialovers)

- Instagram: [@almerialovers](https://www.instagram.com/almerialovers/)

- Youtube: [turismoalmeria channel](https://www.youtube.com/c/turismoalmeria)

### Picture galleries

- [Flickr Album](https://www.flickr.com/photos/franfazer/albums/72157624101393411)

  of Almería province from Francisco Javier Requena.
