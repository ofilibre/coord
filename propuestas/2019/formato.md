# Formato del Congreso

Me gustaría un congreso que recuperase la idea de comunidad, y que se
pudiera repetir al menos unos cuantos años. Que podamos aprovechar la
experiencia (los viejunos) y las ganas de la gente nueva, en un ambiente
técnico, pero que no olvide, y explique, lo importante que es que la
tecnología sea libre.

Yo también iría a un modelo fosdem, con ponencias para todos, y luego
reparto por salas con organización propia, mientras todas las charlas
sean sobre tecnologías libres, y no sean marketinianas.

Trataría de aprovechar las sesiones plenarias para atraer gente con
nombres y temas "que suenen un poco", y podríamos tener un pequeño
presupuesto para traer alguno de ellos (con el modelo de se paga el
viaje y la estancia si no tienes empresa o de verdad de la buena tu
empresa no te lo paga), sólo para algunas "estrellas invitadas".

Creo que un track de "lo que han cambiado las cosas en 20 años" sería
casi obligatorio, viendo lo que se comenta por el otro canal. Y muy
interesante. El resto, llamada abierta de mini-tracks, cada uno de
dos-tres horas, con organización propia.

Todo en un sitio donde se pueda estar todo el día sin salir, porque
será un día muy intenso. Que se pueda desayunar y comer, o ir a tomar
un café (si es posible, patrocinado, pero no es imprescindible). Si
es posible actividades paralelas lúdicas, de networking, sociales para
hijos y acompañantes, estupendo.

Pero lo haría un día de diario (un viernes, por ejemplo) para mantenerlo
fundamentalmente "profesional" (ya sabéis que tiro a aburrido).

En cuanto a tamaño, yo aspiraría a 200-250, si es posible más. Sería
muy bueno poder grabarlo todo, y/o poner por streaming al menos lo
principal. Alguna actividad paralela en plan hacking, tutorials manos
en la masa, etc estaría bien.

Involucrar a grupos de voluntarios en la organización (por ejemplo
estudiantes, pero no solo) sería estupendo.

Y tratar de que sea sobre todo un evento de comunidad, aunque las empresas
puedan patrocinar, y obviamente habrá muchos empleados de empresas.

Ah, y *lightning talks* de 10 min todo el rato, para que se pueda soltar
mucha gente que no se anima a una charla larga, pero seguro que tiene
cosas que contar.

El sitio tendría que tener una cierta población objetivo "local",
pero lo fundamental sería conseguir atraer a gente de todas partes,
quizás con el contexto común del idioma (y para el primer año,
si lo conseguimos a nivel de las españas, mucho hemos conseguido). La
comunicación sería importante, para que la gente que quiera pueda
desplazarse en el día. Obviamente, facilidades para poder alojarse
serían una gran cosa.

La organización no tendría que ser solo del sitio que lo organice, sino
mucho más variada, para asegurar difusión de la idea por muchos sitios,
y continuidad para otros años. Pero la organización local tendría
que ser también razonablemente fuerte, porque la tarea no es pequeña...
