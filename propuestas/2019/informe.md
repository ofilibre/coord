# Informe esLibre 2019.

## Mujeres en el congreso

Como se indica más abajo, la audiencia fue de un 20% de mujeres. En el
congreso en general, la presencia de mujeres fue baja, si bien en la
devroom de privacidad y seguridad había mayoría de ponentes y
asistentes de ese género. No hubo ninguna propuesta específica de
devrooms por parte de comunidades femeninas. 

## Eventos sociales

Aunque se había programado un tour de graffiti para el día después,
realmente no logró tener suficiente audiencia, así que se
suspendió. La KDD el día anterior al evento recibió unas 30 personas
al final, sobre todo a partir de las 10 de la noche. Finalmente no se
organizó una ludoteca para niños, porque realmente tampoco vinieron
tantos. 

## Alcance

De acuerdo
con
[los informes de Twitter](https://gitlab.com/eslibre/coord/issues/19),
se consiguieron 127.400 impresiones en el período del mes que incluyó
esLibre. Este
fue
[el tuit con más impresiones](https://twitter.com/esLibre_/status/1142127243692576768),
el de cierre del evento, con aproximadamente unas 600 impresiones. 

La información de audiencias arroja un 96% interesado en noticias y
tecnología, y un 20% de mujeres, lo que es bajo, pero por otra parte
habitual en eventos tecnológicos, quizás algo superior. El 90% de los
visitantes es desde España, con Andalucía con un 21% como mayor
origen, seguida de la comunidad de Madrid. Este dato viene a indicar
que se logró, sobre todo, una audiencia regional, pero para ser el
primero de este tipo, con la mayoría de los ponentes procedentes de
Andalucía, es más o menos lo que cabe esperar.
