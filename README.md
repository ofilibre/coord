# Coordinación

Este es el proyecto para la coordinación **esLibre**, el congreso sobre tecnologías libres y conocimiento abierto.

Desde aquí se gestionan las propuestas de sedes para las diferentes ediciones. Puedes encontrar la información sobre la edición del año pasado en https://eslib.re/2019/, organizada en Granada.

Se libre de participar en cualquier parte, tanto en los issues que se abran para discutir y coordinar como en el grupo abierto de Telegram: https://t.me/esLibre.

Actualmente está abierta la **["Convocatoria de Sede" para la edición del año 2020](propuestas/2020/README.md)**.
